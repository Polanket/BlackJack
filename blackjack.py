import random


# Game Starts --- Player 1

choice = int
Total = int
cardArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]

first_card = random.choice(cardArray)

if first_card == 1:
    choice = input("You got an Ace! Choose 1 or 11: ")
    if choice == "11":
        start = 11
        Total = start
        print("First card: " + str(start))
    if choice == "1":
        start = 1
        Total = start
        print("First card: " + str(start))
else:
    Total = first_card
    print("First Card: " + str(Total))

second_card = random.choice(cardArray)
if second_card == 1:
    choice = input("You got an Ace! Choose 1 or 11: ")
    if choice == "11":
        start = 11
        Total = Total + start
        print("Second card: " + str(start))
        print("Your total is: " + str(Total))
    elif choice == "1":
        start = 1
        Total = Total + start
        print("Second card: " + str(start))
        print("Your total is: " + str(start))
else:
    Total = Total + second_card
    print("Second Card: " + str(second_card))
    print("Total is: " + str(Total))

another = input("Do you want a third card? (yes or no) ")

if another == "yes":
    third_card = random.choice(cardArray)
    if third_card == 1:
        choice = input("You got an Ace! Choose 1 or 11: ")
    if choice == "11":
        start = 11
        Total = Total + start
        print("Third card: " + str(start))
        print("Your total is: " + str(Total))
    if choice == "1":
        start = 1
        Total = Total + start
        print("Third card: " + str(start))
        print("Your total is: " + str(Total))
    else:
        Total = Total + third_card
        print("Third card: " + str(third_card))
        print("Your total is: " + str(Total))

if Total > 21:
    print("Busted!")
    exit()

another_1 = input("Do you want a fourth card? (yes or no) ")

if another_1 == "yes":
    fourth_card = 1 #random.choice(cardArray)
    if fourth_card == 1:
        choice = input("You got an Ace! Choose 1 or 11: ")
    if choice == "11":
        start = 11
        Total = Total + start
        print("Fourth card: " + str(start))
        print("Your total is: " + str(Total))
    if choice == "1":
        start = 1
        Total = Total + start
        print("Fourth card: " + str(start))
        print("Your total is: " + str(Total))
    else:
        Total = Total + fourth_card
        print("Fourth card: " + str(fourth_card))
        print("Your total is: " + str(Total))

if Total > 21:
    print("Busted!")
    exit()

# Game Starts --- Dealer

input("Press enter to check dealer's cards")

firstcard = random.choice(cardArray)
print("Dealer's First: " + str(firstcard))

secondcard = random.choice(cardArray)
print("Dealer's Second: " + str(secondcard))

sum = firstcard + secondcard
print("Dealer's Total: " + str(sum))

if sum <= Total:
    third_card = random.choice(cardArray)
    sum = sum + third_card
    print("Dealer's Third: " + str(third_card))
    print("Dealer's Total: " + str(sum))


# Did I Win?

if (Total > sum and Total <= 21) or (sum > 21 and Total < 21):
    print("You win")
elif (Total < sum and sum <= 21) or (sum < 21 and Total > 21):
    print("You lose")
elif (sum > 21 and Total > 21):
    print("No one wins")
elif sum == Total:
    print("Tied Game")
